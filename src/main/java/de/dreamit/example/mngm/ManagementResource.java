package de.dreamit.example.mngm;

import de.dreamit.example.SenderService;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

@Named
@Stateless
@Path("/_mgmt")
public class ManagementResource {

    private static Logger log = Logger.getLogger(ManagementResource.class.getName());

    @Inject
    private SenderService senderService;

    @GET
    @Produces("text/event-stream")
    @Path("hystrix.stream")
    public EventOutput hystrixVertxStream() {
        final EventOutput eventOutput = new EventOutput();

        CompletableFuture.runAsync(() -> {
            EventBus eventBus = senderService.getVertx().eventBus();
            eventBus.consumer("vertx.circuit-breaker").handler(message -> {
                try {
                    JsonObject json = HystrixMetricEventUtils.build((JsonObject) message.body());
                    String msg = json.encode();
                    sendServerDataEvent(eventOutput, msg);
                    eventOutput.close();
                } catch (Exception ex) {
                    log.warning("Error closing event output");
                }
            });
        });

        return eventOutput;
    }

    private void sendServerDataEvent(EventOutput eventOutput, String data) {
        try {
            final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
            eventBuilder.data(String.class, data);
            final OutboundEvent event = eventBuilder.build();
            eventOutput.write(event);
        } catch (IOException e) {
            log.warning("Error when writing the event" + e.getMessage());
        }
    }
}
