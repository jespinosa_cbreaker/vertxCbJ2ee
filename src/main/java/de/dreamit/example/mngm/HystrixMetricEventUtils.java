package de.dreamit.example.mngm;

import io.vertx.circuitbreaker.CircuitBreakerState;
import io.vertx.core.json.JsonObject;

/**
 * Created by jes on 22/05/2017.
 */
public class HystrixMetricEventUtils {

    public static JsonObject build(JsonObject body) {
        String state = body.getString("state");
        JsonObject json = new JsonObject();
        json.put("type", "HystrixCommand");
        json.put("name", body.getString("name"));
        json.put("group", body.getString("node"));
        json.put("currentTime", Long.valueOf(System.currentTimeMillis()));
        json.put("isCircuitBreakerOpen", Boolean.valueOf(state.equalsIgnoreCase(CircuitBreakerState.OPEN.toString())));
        json.put("errorPercentage", body.getInteger("totalErrorPercentage", Integer.valueOf(0)));
        json.put("errorCount", body.getInteger("totalErrorCount", Integer.valueOf(0)));
        json.put("requestCount", body.getInteger("totalOperationCount", Integer.valueOf(0)));
        json.put("rollingCountCollapsedRequests", body.getInteger("rollingOperationCount", Integer.valueOf(0)));
        json.put("rollingCountExceptionsThrown", body.getInteger("rollingExceptionCount", Integer.valueOf(0)));
        json.put("rollingCountFailure", body.getInteger("rollingFailureCount", Integer.valueOf(0)));
        json.put("rollingCountTimeout", body.getInteger("rollingTimeoutCound", Integer.valueOf(0)));
        json.put("rollingCountFallbackFailure", body.getInteger("rollingFallbackFailureCount", Integer.valueOf(0)));
        json.put("rollingCountFallbackRejection", body.getInteger("fallbackRejection", Integer.valueOf(0)));
        json.put("rollingCountFallbackSuccess", body.getInteger("rollingFallbackSuccessCount", Integer.valueOf(0)));
        json.put("rollingCountResponsesFromCache", Integer.valueOf(0));
        json.put("rollingCountSemaphoreRejected", Integer.valueOf(0));
        json.put("rollingCountShortCircuited", body.getInteger("rollingShortCircuitedCount", Integer.valueOf(0)));
        json.put("rollingCountSuccess", body.getInteger("rollingSuccessCount", Integer.valueOf(0)));
        json.put("rollingCountThreadPoolRejected", Integer.valueOf(0));
        json.put("rollingCountTimeout", body.getInteger("rollingTimeoutCount", Integer.valueOf(0)));
        json.put("currentConcurrentExecutionCount", Integer.valueOf(0));
        json.put("latencyExecute_mean", body.getInteger("rollingLatencyMean", Integer.valueOf(0)));
        json.put("latencyExecute", body.getJsonObject("rollingLatency", new JsonObject()));
        json.put("propertyValue_circuitBreakerRequestVolumeThreshold", Integer.valueOf(0));
        json.put("propertyValue_circuitBreakerSleepWindowInMilliseconds", body.getLong("resetTimeout", Long.valueOf(0L)));
        json.put("propertyValue_circuitBreakerErrorThresholdPercentage", Integer.valueOf(0));
        json.put("propertyValue_circuitBreakerForceOpen", Boolean.valueOf(false));
        json.put("propertyValue_circuitBreakerForceClosed", Boolean.valueOf(false));
        json.put("propertyValue_circuitBreakerEnabled", Boolean.valueOf(true));
        json.put("propertyValue_executionIsolationStrategy", "THREAD");
        json.put("propertyValue_executionIsolationThreadTimeoutInMilliseconds", body.getLong("timeout", Long.valueOf(0L)));
        json.put("propertyValue_executionIsolationThreadInterruptOnTimeout", Boolean.valueOf(true));
        json.put("propertyValue_executionIsolationThreadPoolKeyOverride", "");
        json.put("propertyValue_executionIsolationSemaphoreMaxConcurrentRequests", Integer.valueOf(0));
        json.put("propertyValue_fallbackIsolationSemaphoreMaxConcurrentRequests", Integer.valueOf(0));
        json.put("propertyValue_metricsRollingStatisticalWindowInMilliseconds", body.getLong("metricRollingWindow", Long.valueOf(0L)));
        json.put("propertyValue_requestCacheEnabled", Boolean.valueOf(false));
        json.put("propertyValue_requestLogEnabled", Boolean.valueOf(false));
        json.put("reportingHosts", Integer.valueOf(1));
        return json;
    }
}
