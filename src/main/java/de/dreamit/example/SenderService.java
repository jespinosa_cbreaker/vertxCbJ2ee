package de.dreamit.example;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Vertx;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.config.ConfigException;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@ApplicationScoped
public class SenderService {

    private static final Logger logger = Logger.getLogger(SenderService.class.getSimpleName());

    private Producer<String, String> producer;

    private CircuitBreaker circuitBreaker;

    private Vertx vertx;

    private AtomicInteger number = new AtomicInteger();

    public SenderService() {
        vertx = Vertx.vertx();
        createProducer();
        createCircuitBreaker();
    }

    private void createCircuitBreaker() {
        circuitBreaker = CircuitBreaker.create("vertx.circuit-breaker", getVertx(),
                new CircuitBreakerOptions()
                        .setMaxFailures(5) // number of failure before opening the circuit
                        .setTimeout(400) // consider a failure if the operation does not succeed in time
                        .setFallbackOnFailure(false) // do we call the fallback on failure
                        .setResetTimeout(15000)); // time spent in open state before attempting to re-try
    }

    public void produce(String value) {
        if(producer != null && circuitBreaker != null) {
            String topic = "tracking";
            circuitBreaker.execute(future -> {
                java.util.concurrent.Future<RecordMetadata> send = producer.send(new ProducerRecord<>(topic, value));
                try {
                    future.complete(send.get(200, TimeUnit.MILLISECONDS));
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void createProducer() {
        close();
        try {
            producer = new KafkaProducer<>(getProperties());
        } catch (ConfigException e) {
            logger.log(Level.SEVERE, "Error configuring kafka producer: " + e.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Error creating kafka producer: " + e.getMessage());
        }
    }

    private Properties getProperties() {
        Properties params = new Properties();
        params.put("bootstrap.servers", "localhost:9092");
        params.put("acks", "0");
        params.put("retries", 0);
        params.put("batch.size", 16384);
        params.put("linger.ms", 1);
        params.put("buffer.memory", 33554432);
        params.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        params.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        params.put("max.block.ms", 200L);
        params.put("metadata.fetch.timeout.ms", 200L);
        params.put("metadata.max.age.ms", 300000L);
        return params;
    }


    @PreDestroy
    void close() {
        if (producer != null) {
            try {
                producer.close();
            } catch (Exception e) {
                logger.warning("Error closing producer: " + e.getMessage());
            }
        }
        if (circuitBreaker != null) {
            try {
                circuitBreaker.close();
            } catch (Exception e) {
                logger.warning("Error closing circuit breaker: " + e.getMessage());
            }
        }
    }

    public Vertx getVertx() {
        return vertx;
    }

    public Integer getNumber() {
        return number.incrementAndGet();
    }
}
